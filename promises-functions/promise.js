// definimos la Promise
const realizarVenta = new Promise((resolve, reject) => {
    const venta = false;
    if(venta) {
        resolve('La venta se ha realizado con éxito.');
    } else {
        reject('No se ha podido completar la transacción.');
    }
})

// funcion que recibe como parametro un mensaje y lo imprime en la consola
const imprimirMensaje = (message) => {
    console.log('Mensaje:', message);
}

// trabajamos con el resultado de la Promise
realizarVenta
    .then(message => imprimirMensaje(message))
    .catch(error => imprimirMensaje(error));