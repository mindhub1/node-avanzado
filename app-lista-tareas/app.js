const colors = require('colors');
const { mostrarMenu, pausa, leerInput, listadoTareasBorrar, confirm, mostrarListadoChecklist } = require('./helpers/inquirer');
const { Tareas } = require('./models/tareas');
const { guardarDB, leerDB } = require('./helpers/guardarArchivo');

console.clear();

const main = async () => {
    let option = '';
    const listaTareas = new Tareas();

    const data = leerDB();
    listaTareas.cargarTareasFromArray(data);

    do {

        option = await mostrarMenu();
        
        switch(option) {

            case '1': // crear tarea
                let desc = await leerInput();
                listaTareas.crearTarea(desc);
                await guardarDB(listaTareas.listadoArr)
                    .then(message => console.log(`Tarea agregada! \n${message}\n`))
                    .catch(err => console.log(err))
                break;

            case '2': // listar todas las tareas
                listaTareas.listadoCompleto();
                break;
            
            case '3': // listar tareas completadas
                listaTareas.listarPendientesCompletadas(true);
                break;

            case '4': // listar tareas pendientes
                listaTareas.listarPendientesCompletadas(false);
                break;

            case '5':
                const arr = await mostrarListadoChecklist(listaTareas.listadoArr);
                listaTareas.toggleCompletadas(arr.ids);
                break;

            case '6':
                const idBorrar = await listadoTareasBorrar(listaTareas.listadoArr);
                if(idBorrar.borrar !== 0) {
                    const confirmarBorrado = await confirm('Estas seguro de borrar la tarea?');
                    if(confirmarBorrado) {
                        listaTareas.borrarTarea(idBorrar.borrar);
                        console.log('La tarea fue borrada.\n');
                    } else {
                        console.log('La tarea no fue borrada.\n');
                    }
                }
                break;

        }

        await pausa();
        
    } while(option !== '0');
    
}

main();
