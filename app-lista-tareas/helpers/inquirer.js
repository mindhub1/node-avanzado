const inquirer = require('inquirer');

const mostrarMenu = () => {
    console.clear();
    console.log(`${'\n----------------------------'.rainbow}\n   Seleccione una Opcion   \n${'----------------------------'.rainbow}`);

    const question = [
        {
            type: 'list',
            name: 'options',
            message: 'Qué desea hacer? ',
            choices: [
                { value: '1', name: `${'1)'.yellow} Crear tarea` },
                { value: '2', name: `${'2)'.yellow} Listar tareas` },
                { value: '3', name: `${'3)'.yellow} Listar tareas completadas` },
                { value: '4', name: `${'4)'.yellow} Listar tareas pendientes` },
                { value: '5', name: `${'5)'.yellow} Completar tarea(s)` },
                { value: '6', name: `${'6)'.yellow} Borrar tarea` },
                { value: '0', name: `${'0)'.yellow} Salir` },
            ],
        }
    ];

    return new Promise((resolve, reject) => {
        inquirer
            .prompt(question)
            .then((res) => {
                resolve(res.options);
            })
            .catch((error) => {
                reject(error);
            });
    })
}


const pausa = async () => {
    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `Presione ${'ENTER'.yellow} para continuar`
        }
    ];
    await inquirer.prompt(question);
}



const leerInput = async () => {
    const question = [
        {
            type: 'input',
            name: 'desc',
            message: 'Descripción: ',
            validate( value ){
                if (value.length === 0) {
                    return 'Por favor ingrese un valor';
                }
                return true;
            }
        }
    ];
    const { desc } = await inquirer.prompt(question);
    return desc;
}


const listadoTareasBorrar = async (listado) => {
    const choices = listado.map((tarea, i) => {
        const estado = tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red;
        return {
            value: tarea.id,
            name: `${i + 1}. `.red + `${tarea.desc}:  ${estado}`
          };
      
    });
    choices.unshift({ value: 0, name: '0. '.red + 'Cancelar' })

    const question = [
        {
            type: 'list',
            name: 'borrar',
            message: 'Seleccione la tarea que desea borrar: ',
            choices
        }
    ];

    return await inquirer.prompt(question);
}


const confirm = async (message) => {
    const question = [
      {
        type: 'confirm',
        name: 'ok',
        message
      }
    ]
    const { ok } = await inquirer.prompt(question)
    return ok
}


const mostrarListadoChecklist = async (listado) => {
    const choices = listado.map((tarea, i) => {
        const estado = tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red;
        return {
            value: tarea.id,
            name: `${i + 1}. `.red + `${tarea.desc}:  ${estado}`,
            checked: tarea.completadoEn ? true : false
          };
      
    });

    const question = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Selecciones',
            choices
        }
    ];

    return await inquirer.prompt(question);
}
  


module.exports = {
    mostrarMenu,
    pausa,
    leerInput,
    listadoTareasBorrar,
    confirm,
    mostrarListadoChecklist
}
