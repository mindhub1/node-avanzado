const colors = require('colors');
const readline = require('readline');

const mostrarMenu = () => {
    return new Promise((resolve, reject) => {
        console.log(colors.rainbow('----------------------------'));
        console.log(colors.bold('   Seleccione una Opcion   '));
        console.log(colors.rainbow('----------------------------'));
        
        console.log(colors.bold('\n¿Qué desea hacer?'))
        console.log(`${'1)'.yellow} Crear tarea`);
        console.log(`${'2)'.yellow} Listar tareas`);
        console.log(`${'3)'.yellow} Listar tareas completadas`);
        console.log(`${'4)'.yellow} Listar tareas pendientes`);
        console.log(`${'5)'.yellow} Completar tarea(s)`);
        console.log(`${'6)'.yellow} Borrar tarea`);
        console.log(`${'0)'.yellow} Salir`);
        
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        
        rl.question('Seleccione una opcion: \n', (answer) => {
            if(answer < 0 || answer > 6) {
                reject('Opcion invalida: Debe elegir una opcion entre 0 y 6');
            } else {
                resolve(answer);
                rl.close();
            }
        });
    });
};

const pausa = () => {
    return new Promise((resolve) => {
        const rl2 = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        
        console.log('\n')
        rl2.question(`Presione ${colors.cyan('ENTER')} para continuar `, (answer) => {
            resolve();
            rl2.close();
            console.clear();
        });
    });
};


module.exports = {
    mostrarMenu, 
    pausa
};