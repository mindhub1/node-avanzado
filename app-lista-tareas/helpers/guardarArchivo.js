const fs = require('fs');

const path = './database/tareas.json';

const guardarDB = (data) => {
    return new Promise((resolve, reject) => {
        const jsonData = JSON.stringify(data);

        fs.writeFile(path, jsonData, (err) => {
            reject(err => { throw err });
        });

        resolve('El archivo tareas.json ha sido guardado!');
    })
}


const leerDB = () => {
    if(fs.existsSync(path)) {
        let strData = fs.readFileSync(path, {encoding: 'utf-8'});
        let jsonData = JSON.parse(strData);
        return jsonData;
    }
    return null;
}

module.exports = {
    guardarDB,
    leerDB
}