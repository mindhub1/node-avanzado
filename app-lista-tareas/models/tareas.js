const { Tarea } = require('./tarea');

class Tareas {

    constructor () {
        this.listado = {}
      }    
      
    crearTarea(desc) {
        const tarea = new Tarea(desc);
        this.listado[tarea.id] = tarea;
    }

    get listadoArr() {
        const arr = [];
        Object.keys(this.listado).forEach(key => {
            arr.push(this.listado[key]);
        });
        return arr;
    }

    cargarTareasFromArray(data) {
        data.forEach(elem => {
            this.listado[elem.id] = elem;
        });
    }

    listadoCompleto() {
        this.listadoArr.forEach((tarea, i) => {
            const id = (i+1).toString();
            const desc = tarea.desc;
            const estado = tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red;
            
            console.log(`${id.yellow} ${desc} :: ${estado}`);
        })
    }

    listarPendientesCompletadas(completadas = true) {
        let arr = [];
        if(completadas) {
            arr = this.listadoArr.filter(tarea => tarea.completadoEn != null);
            if(arr.length == 0) console.log('Aun no hay tareas completadas.\n');
        } else {
            arr = this.listadoArr.filter(tarea => tarea.completadoEn == null);
            if(arr.length == 0) console.log('No quedan hay tareas pendientes.\n');
        }

        arr.forEach((tarea, i) => {
            const id = (i+1).toString();
            const desc = tarea.desc;
            const estado = tarea.completadoEn ? 'Completada'.green : 'Pendiente'.red;
            
            console.log(`${id.yellow} ${desc} :: ${estado}`);
        })
    }

    borrarTarea(id) {
        if(this.listadoArr.filter(obj => obj.id == id).length > 0) { // si el array filtrado por id tiene mas de 0 elementos entonces el id si existe
            delete this.listado[id];
        } else {
            throw new Error('La tarea no existe');
        }
        return this.listadoArr;
    }

    toggleCompletadas(ids) {

        this.listadoArr.forEach(tarea => {
            if(ids.includes(tarea.id)) {
                this.listado[tarea.id].completadoEn = new Date().toISOString();
            } else {
                this.listado[tarea.id].completadoEn = null;
            }
        });
    }
}



module.exports = {
    Tareas
}