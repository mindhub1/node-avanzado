const { v4: uuidv4 } = require('uuid');

class Tarea {
    id = ''; // id unico de la tarea
    desc = '';  // descripcion de la tarea
    completadoEn = null;  // fecha en que se completo la tarea

    constructor(desc) {
        this.id = uuidv4();
        this.desc = desc;
    }

}

module.exports = {
    Tarea
}