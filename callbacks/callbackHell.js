const diasHabiles = [];


// funcion Callback. Muestra todos los elementos del array diasHabiles
const mostrarDiasHabiles = () => {
    console.log(diasHabiles);
    console.log('\n');
}


// funcion que agrega elementos al array diasHabiles
const agregarDiaHabil = (diaHabil, callback) => {
    diasHabiles.push(diaHabil);
    callback();
}


// CallbackHell que va a agregando elemento por elemento con un timeOut
const iniciarCallbackHell = () => {
    setTimeout(() => {
        agregarDiaHabil('Lunes', mostrarDiasHabiles);

        setTimeout(() => {
            agregarDiaHabil('Martes', mostrarDiasHabiles);

            setTimeout(() => {
                agregarDiaHabil('Miercoles', mostrarDiasHabiles);

                setTimeout(() => {
                    agregarDiaHabil('Jueves', mostrarDiasHabiles);

                    setTimeout(() => {
                        agregarDiaHabil('Viernes', mostrarDiasHabiles);
                    }, 1000);

                }, 1000);

            }, 1000);

        }, 1000);

    }, 1000);
}

iniciarCallbackHell();