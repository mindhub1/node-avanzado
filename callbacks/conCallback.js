const modulosVistos = ['Modulo 1: Introduccion', 'Modulo 2: Fundamentos de Node'];

// funcion callback. Muestra los elementos del array modulosVistos
const mostrarModulos = () => {
    setTimeout(() => {
        modulosVistos.forEach(modulo => console.log(modulo));
    }, 1000);
};


// funcion que agrega un nuevo elemento al array modulosVistos
const nuevoModulo = (modulo, callback) => {
    setTimeout(() => {
        modulosVistos.push(modulo);
        callback();
    }, 2000);
};

// llamar a la funcion nuevoModulo con dos parametros: el nuevo modulo a agregar y la funcion callback respectivamente
nuevoModulo('Modulo 3: Node Avanzado', mostrarModulos);