const modulosVistos = ['Modulo 1: Introduccion', 'Modulo 2: Fundamentos de Node'];

// funcion que muestra los elementos del array modulosVistos
const mostrarModulos = () => {
    setTimeout(() => {
        modulosVistos.forEach(modulo => console.log(modulo));
    }, 1000);
}

mostrarModulos();