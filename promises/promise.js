// definimos la promesa con su respectiva condicion, y dependiendo de esta ultima la promesa se resolverá con éxito (resolve) o fallará (reject)
const alquilarPropiedad =  new Promise ((resolve, reject) => {
    const propiedadAlquilada = false;
    if(propiedadAlquilada) {
        resolve('La propiedad ha sido alquilada.');
    } else {
        reject('El dueño de la propiedad y el inquilino no pudieron llegar a un acuerdo.');
    }
});

// trabajamos con el resultado de la Promise. Si la Promise se resolvió entonces se ejecuta el .then, y si la Promise falló entonces se ejecuta el .catch
alquilarPropiedad
    .then(message => console.log(message))
    .catch(error => console.log(error));